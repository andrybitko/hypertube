window.fbAsyncInit = function() {
    FB.init({
        appId      : '553802811693930',
        cookie     : true,
        xfbml      : true,
        version    : 'v3.3'
    });

    FB.AppEvents.logPageView();
    checkLoginStatus();

};

(function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


//для отримання стану реєстрації
function checkLoginStatus() {
    FB.getLoginStatus(function(response) {
        console.log(response);
        //statusChangeCallback(response);
        if (response.status === 'connected') {
            console.log(response.authResponse.accessToken);
            getFbUserData();
            //location.replace("http://localhost:8100");
        }

    });
}


function getFbUserData(){
    FB.api('/me', {locale: 'en_US', fields: 'id,first_name,last_name,email,picture'},
        function (response) {
            // document.getElementById('fbLink').setAttribute("onclick","fbLogout()");
            // document.getElementById('fbLink').innerHTML = 'Logout from Facebook';
            // document.getElementById('status').innerHTML = 'Thanks for logging in, ' + response.first_name + '!';
            // document.getElementById('userData').innerHTML = '<p><b>FB ID:</b> '+response.id+'</p><p><b>Name:</b> '+response.first_name+' '+response.last_name+'</p><p><b>Email:</b> '+response.email+'</p><p><b>Picture:</b> <img src="'+response.picture.data.url+'"/></p>';

            saveUserData(response);
        });


}

function saveUserData(userData){
    var picture = userData.picture.data.url;
    delete userData['picture'];
    var parametr = 'facebook/' + JSON.stringify(userData);

    $.get('http://localhost:8100/user/savefacebook/' + parametr, function(data){ return true; });

}
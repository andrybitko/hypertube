var ajax_loader = $('#ajax_loader');
var search_input = $('#search_input');
var list_films = $('#list_films');
var show_block = $('#show_block');
var global_array = "";

// global function <
function delay(fn, ms) {
    let timer = 0
    return function(...args) {
        clearTimeout(timer)
        timer = setTimeout(fn.bind(this, ...args), ms || 0)
    }
}
// end />

function get_count_films(action = false) {
    var all_films = document.querySelectorAll("div.movie_block[data-id]");
    if (action)
        return (all_films);
    return (all_films.length);
}

function pagination(data)
{
    var count = 0;
    var res = data.slice();
    $.each(data, function(i, item)
    {
        if (item)
        {
            if (count == 14)
            {
                if (data.length)
                    show_block.show();
                return false;
            }
            list_films.append("<div data-id=\"" + item.imdb_code + "\"" +
                " class=\"col-xs-12 col-sm-12 col-md-6 col-lg-6 movie_block\"> " +
                "<div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6 movie_block_2\">" +
                " <a href=\"/browse/main/" + item.imdb_code + "\"> <img src=\"" + item.poster + "\"" +
                " class=\"film_photo img-fluid\"></a></div><div class=\"col-xs-6 col-sm-6 col-md-6 col-lg-6\"> " +
                "<a href=\"/browse/main/" + item.imdb_code + "\"> <h4>" + item.title + "</h4> </a> <p> " +
                "<span class=\"span_class\">"+text_rating+": </span> " +
                "<a target=\"_blank\" href=\"https://www.imdb.com/title/" + item.imdb_code + "\">" + item.rating + "</a>" +
                " </p> <p> <span class=\"span_class\">"+text_year+": </span> " + item.year + " </p> " +
                "<span class=\"span_class\">"+text_short_description+": </span> <p style=\"width: 100%; height: 80px; overflow: hidden;\"> " + item.synopsis + "" +
                " </p> <a href=\"/browse/main/" + item.imdb_code + "\" class=\"btn btn-default\">"+text_watch+"</a> </div> </div>")
            res.shift();
            count++;
        }
    })

    global_pagination = res;
    if (!global_pagination.length)
        show_block.hide();
}

function getAndPrint(data) {
    $.ajax({
        type: 'POST',
        url: '/get_films',
        data,
        error: function(xhr, textStatus, error) {
            alert("Error: " + error);
        },
        success: function(data) {
            if (data.length)
            {
                global_array = data.slice();
                pagination(data);
            }
            else
            {
                global_array = "";
                list_films.append("<h3>Fims not found</h3>");
            }
            ajax_loader.hide();
        },
        dataType: 'json'
    });
}

$(document).ready(function() {
    if (!(get_count_films())) {
        data = {action: "main"};
        ajax_loader.show();
        getAndPrint(data);
    }

    search_input.keyup(delay(function(e) {
        show_block.hide();
        list_films.html('');
        ajax_loader.show();
        data = {action: "search",word: search_input.val()};
        getAndPrint(data);
    }, 1000));

    $('.sort_button').on('click', function()
    {
        show_block.hide();
        if ($(this).val() == 'title' || $(this).val() == 'year' || $(this).val() == 'rating')
        {
            if (global_array.length)
            {
                list_films.html('');
                ajax_loader.show();
                data = {action: "sort",param: $(this).val(),res: global_array};
                getAndPrint(data);
            }
        }
    });
    $('.show_button').on('click', function()
    {
        pagination(global_pagination);
    });
});

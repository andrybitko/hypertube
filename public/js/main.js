$(document).ready(function()
{
    var name_film = $('#name_film').text();

    $('.set_quality').on('click', function()
    {
        if ($(this).text() == 720 || $(this).text() == 1080)
        {
            $('.set_quality').attr("disabled", false);
            $(this).attr("disabled", true);
            if ($("#mp4_src").attr("src"))
            {
                if ($("#sub_en").attr("src"))
                    $("#sub_en").attr("src", "");
                if ($("#sub_ru").attr("src"))
                    $("#sub_ru").attr("src", "");
                $("#mp4_src").attr("src", "");
                $("#video_block")[0].load();
            }
            $("#video_block").removeClass("opacity");
            $("#video_block").addClass("loading");
            $("#video_block").attr("poster", "data:image/gif,AAAA");
            getHashFilm(name_film, $(this).text());
        }
    });

    var video = document.getElementById('video_block');
    var timeCopy = 0;

    video.addEventListener('timeupdate', function() {
        if (!video.seeking) {
            timeCopy = video.currentTime;
        }
    })

    video.addEventListener('seeking', function() {
        if (video.currentTime - timeCopy > 0) {
            video.currentTime = timeCopy;
        }
    })
});

function getHashFilm(name, quality)
{
    if (name && quality)
    {
        $.ajax({
            type: 'POST',
            url: '/internalpuroses',
            data:
                {
                    name: name,
                    quality: quality,
                },
            error: function(xhr, textStatus, error)
            {
                alert("Error :" + error);
            },
            success: function (data)
            {
                if (data['hash'] && data['size'])
                {
                    sendHashPlayFilm(data['hash'], quality);
                }
            },
            dataType: 'json'
        });
    }
}

function sendHashPlayFilm(hash, quality)
{
    if (hash && quality)
    {
        $.ajax({
            type: 'POST',
            url: 'http://localhost:5000',
            data:
                {
                    id: id_dd,
                    hash: hash,
                    resolution: quality,
                },
            error: function(xhr, textStatus, error)
            {

            },
            success: function (data)
            {
                if (data['link'])
                {
                    var site = "http://localhost:8101";
                    data['link'] = site + data['link'];
                    if (data['en'])
                        $("#sub_en").attr("src", data['en']);
                    if (data['ru'])
                        $("#sub_ru").attr("src", data['ru']);
                    $("#mp4_src").attr("src", data['link']);
                    $("#video_block").removeClass("loading");
                    $("#video_block")[0].load();
                    $("#video_block")[0].play();

                    $.ajax({
                        type: 'POST',
                        url: '/pass-link',
                        data:
                            {
                                hashFolder: data['hashFolder'],
                                link: data['link']
                            },
                        error: function(xhr, textStatus, error)
                        {

                        },
                        success: function (data)
                        {

                        }
                    });
                }
            },
            dataType: 'json'
        });
    }
}

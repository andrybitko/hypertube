<?php

namespace App\Controllers;

use App\Components\Imdb;
use App\Models\Movie;
use Slim\Views\Twig as View;
//use App\Models\User;

class HomeController extends Controller
{
	public function index($request, $response)
	{
        Movie::deleteMovieLink(2592000);
		return $this->container->view->render($response, 'home.twig');
	}
}

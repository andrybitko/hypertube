<?php

namespace App\Controllers;

class Controller
{
	protected $container;

	public function __construct($container){
		$this->container = $container;
	}

    public static function performCurl($url, $params = false, $headers = false)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        if ($params)
        {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
        }
        if ($headers)
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($ch);
        curl_close ($ch);
        return ($result);
    }

}

-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3307
-- Generation Time: Jun 02, 2019 at 07:37 AM
-- Server version: 8.0.15
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hypertube`
--
CREATE DATABASE IF NOT EXISTS `hypertube` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `hypertube`;
-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `movie_id` varchar(255) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `user_id` int(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`movie_id`, `comment`, `user_id`) VALUES
('tt5936578', '2', 0),
('tt5936578', '2', 0),
('tt5936578', '2', 0),
('tt5936578', '2', 0),
('tt5936578', '2', 0),
('tt5936578', '123', 2),
('tt5936578', '123', 2),
('tt5936578', '123fdsfsdfsdfsd', 2),
('tt5936578', '123fdsfsdfsdfsd', 2),
('tt5936578', 'dfsfsdfds', 2),
('tt5936578', 'fdsfdsfdsfdsfdsfds', 2),
('tt5936578', '&lt;input&gt;fdfdsf&lt;/input&gt;', 2),
('tt5936578', '<input>fdsjkfjdsk</input>', 2),
('tt0098724', 'ddefefsf fe fef feesa fea faef aef a', 10),
('tt0098724', 'ge . eg e', 10),
('tt0446059', 'grgr g', 10),
('tt0446059', 'grgr g', 10),
('tt0054144', 'asdf', 14),
('tt0054144', 'fdsfds', 14),
('tt0054144', 'sdfdsfds', 14),
('tt0054144', 'fsdfsdfds', 15),
('tt0054144', 'fdsfsdfsd', 15),
('tt0054144', 'fdsfsdfsd', 15),
('tt0054144', 'fsdfsdf', 15),
('tt0054144', 'fdsfsdfds', 15),
('tt0054144', 'feefee', 15),
('tt0054144', 'fsdfdsfds', 15),
('tt0054144', 'fdsfsdfsd', 15),
('tt0054144', '11111', 15),
('tt0055256', 'fefefef', 17),
('tt0055256', 'fefef fe fe fe ', 17),
('tt0054144', 'll', 17);

-- --------------------------------------------------------

--
-- Table structure for table `movies`
--

CREATE TABLE `movies` (
  `link` varchar(255) DEFAULT NULL,
  `time` int(10) DEFAULT NULL,
  `uid` int(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(55) NOT NULL,
  `login` varchar(55) NOT NULL,
  `social` int(11) NOT NULL DEFAULT '0',
  `social_id` bigint(20) NOT NULL DEFAULT '0',
  `password` varchar(255) NOT NULL,
  `fullname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `lastname` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `email` varchar(55) NOT NULL,
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT 'default.jpg',
  `token` varchar(55) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `login`, `social`, `social_id`, `password`, `fullname`, `name`, `lastname`, `email`, `avatar`, `token`, `active`) VALUES
(2, 'yaroslav', 0, 0, '$2y$10$4BeWTz30JLLmBz8THA1iEOzVFKJc7qKJ368GNNlOCE.LKiJ2L1D5u', '', 'Yaroslav', 'fdsfsds1', 'invo.yarik@gmail.com', 'neon_genesis_evangelion_asuka_langley_i-wallpaper-2560x1440.jpg', '15d98818ed43f21c2398', 0),
(7, '0', 2, 29527, '0', 'Yaroslav Skoroden', '', '', 'yskorode@student.unit.ua', NULL, 'da7a8896ba9e51c1bb54', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `movies`
--
ALTER TABLE `movies`
  ADD UNIQUE KEY `link` (`link`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(55) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

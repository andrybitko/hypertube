<?php

use App\Middleware\AuthMiddleware;
use App\Middleware\GuestMiddleware;
use App\Middleware\ConfirmMiddleware;
use App\Middleware\CsrfViewMiddleware;

$app->get('/', 'HomeController:index')->setName('home');

$app->group('', function () {
    $this->get('/user/signup', 'UserController:getSignUp')->setName('user.signup');
    $this->post('/user/signup', 'UserController:postSignUp');

    $this->get('/user/signin', 'UserController:getSignIn')->setName('user.signin');
    $this->post('/user/signin', 'UserController:postSignin');

    $this->get('/user/account/confirm/{email:[a-zA-z0-9.-]+\@[a-zA-z0-9.-]+.[a-zA-Z]+}&{token:[a-zA-z0-9.-]+}', 'UserController:getToken');

    $this->get('/user/account/recover/{email:[a-zA-z0-9.-]+\@[a-zA-z0-9.-]+.[a-zA-Z]+}&{token:[a-zA-z0-9.-]+}', 'UserController:getRecover');
    $this->get('/user/account/recoverpass/{email:[a-zA-z0-9.-]+\@[a-zA-z0-9.-]+.[a-zA-Z]+}', 'UserController:postRecover')->setName('user.recover');
})->add(new GuestMiddleware($container))->add(new CsrfViewMiddleware($container));


$app->group('', function () {
    $this->get('/user/signout', 'UserController:getSignOut')->setName('user.signout');

    $this->get('/user/mail', 'UserController:getChangeEmail')->setName('user.mail');
    $this->post('/user/mail', 'UserController:postChangeEmail');

    $this->get('/user/password', 'UserController:getChangePassword')->setName('user.password');
    $this->post('/user/password', 'UserController:postChangePassword');

    $this->get('/user/account', 'UserController:getData')->setName('user.account');

    $this->get('/user/account/{id:[0-9]+}', 'UserController:getAva');
    $this->get('/user/account/del/{id}', 'UserController:delPhoto');

    $this->get('/user/accountchange', 'UserController:getChangeData')->setName('user.accountchange');
    $this->post('/user/accountchange', 'UserController:postChangeData');

    $this->get('/user/accountphoto', 'UserController:getPhoto')->setName('user.accountphoto');
    $this->post('/user/accountphoto', 'UserController:postPhoto');

    $this->get('/users/{login}', 'ViewController:getMainPage');

    $this->get('/browse/main/{movie}', 'BrowseController:getMainPage')->setName('find.next');
    $this->post('/browse/main/{movie}', 'BrowseController:postMainPage');

    $this->get('/browse/main', 'BrowseController:index')->setName('main.page');

    $this->get('/watched', 'BrowseController:getWatched')->setName('watched');
})->add(new AuthMiddleware($container));


$app->group('', function () {

    $this->post('/internalpuroses', 'BrowseController:getLink');

    $this->post('/get_films', 'BrowseController:get_films');

    $this->get('/trailers/{movie}', 'BrowseController:getTrailers');

    $this->get('/social/facebook', 'UserController:loginFacebook');
    $this->get('/social/intra', 'UserController:loginIntra');

    $this->post('/pass-link', 'BrowseController:createLinkTime');

    $this->get('/change/lang', 'UserController:ChangeLang');
});

<?php

$LANG = [];

$LANG['lang']				= "Русский";

$LANG['sign_up']			= "Sign up";
$LANG['sign_in']			= "Sign in";
$LANG['login']				= "Login";
$LANG['password']			= "Password";
$LANG['forgot_password']	= "Forgot password ?";
$LANG['change_password']	= "Change password";
$LANG['change']				= "Change";
$LANG['current_password']	= "Current Password";
$LANG['new_password']		= "New Password";
$LANG['submit']				= "Submit";

$LANG['choose_quality']		= "Choose quality";
$LANG['trailers']			= "Trailers";

$LANG['short_description']	= "Short description";
$LANG['language']			= "Language";
$LANG['genres']				= "Genres";
$LANG['year']				= "Year";
$LANG['rating']				= "Rating";
$LANG['add_comment']		= "Add comment";

$LANG['movies_list']		= "Movies list";
$LANG['search']				= "Search";

$LANG['sort_title']			= "Sort by title";
$LANG['sort_rating']		= "Sort by rating";
$LANG['sort_year']			= "Sort by year";
$LANG['show_more']			= "Show more";
$LANG['watch']				= "Watch";
$LANG['sign_out']			= "Sign out";
$LANG['account_info']	 	= "Your account information";

<?php

$LANG = [];

$LANG['lang']				= "English";

$LANG['sign_up']			= "Зарегистрироваться";
$LANG['sign_in']			= "Войти";
$LANG['login']				= "Логин";
$LANG['password']			= "Пароль";
$LANG['forgot_password']	= "Забыли пароль ?";
$LANG['change_password']	= "Изменить пароль";
$LANG['change']				= "Изменить";
$LANG['current_password']	= "Текущий пароль";
$LANG['new_password']		= "Новый пароль";
$LANG['submit']				= "Отправить";

$LANG['choose_quality']		= "Выберите качество";
$LANG['trailers']			= "Трейлеры";

$LANG['short_description']	= "Краткоe описание";
$LANG['language']			= "Язык";
$LANG['genres']				= "Жанр";
$LANG['year']				= "Год";
$LANG['rating']				= "Рейтинг";
$LANG['add_comment']		= "Добавить комментарий";

$LANG['movies_list']		= "Список фильмов";
$LANG['search']				= "Поиск";

$LANG['sort_title']			= "По названию";
$LANG['sort_rating']		= "По рейтингу";
$LANG['sort_year']			= "По году";
$LANG['show_more']			= "Показать больше";
$LANG['watch']				= "Смотреть";
$LANG['sign_out']			= "Выйти";
$LANG['account_info']	 	= "Информация о вашей учетной записи";

<?php

namespace App\Middleware;

class AuthMiddleware extends Middleware
{
	public function __invoke($request, $response, $next)
	{
		if (!$this->container->auth->check()) {
			$this->container->flash->addMessage('error', 'You are not allowed to get here.');
			return $response->withRedirect($this->container->router->pathFor('user.signin'));
		}

		$response = $next($request, $response);
		return $response;
	}
}
